import 'package:connect_4_flutter/player.dart';
import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class Connect4Engine {
  static Logger logger = Logger();
  static int _columnCount = 7, _rowCount = 6;
  static var _gameContext;
  // This is for testing purpose to check the cell number
  static List<List> _gridCellsLabel;
  static Player _player1;
  static Player _player2;
  // This variable will track current player
  static Player _currentPlayer;

  // This varibale represents a player to represent invalid position
  static Player _outOfBoundsPlayer = Player();

  // This variable will be used to track state of a cell
  // If this is null it means that the cell is empty
  static List<List> _gridCellsState;

  // This list will be used to for updating state incase of BOT
  static List _gridColumns;

  // This variable will track if a game is over or not
  static bool _isGameOver;

  Connect4Engine() {
    logger.i('Called Connect4Engine');
    _isGameOver = false;
    _gridCellsLabel = [];
    _gridCellsState = [];
    _gridColumns = [];
    _player1.addOppenent(_player2);
    _player2.addOppenent(_player1);

    _currentPlayer.toggleTurn();
    for (int column = 0; column < _columnCount; column++) {
      _gridColumns.add(null);
      List<String> _gridColumnLabel = [];
      List _gridColumnState = [];
      _gridCellsLabel.add(_gridColumnLabel);
      _gridCellsState.add(_gridColumnState);
      for (int row = 0; row < _rowCount; row++) {
        _gridColumnLabel.add('$column,$row');
        _gridColumnState.add(null);
      }
    }
    logger.d(_gridCellsLabel);
    logger.d(_gridCellsState);
    // changeToTestState();
  }

  static setGameMode(bool botplay) {
    logger.i('Setting game mode botplay:$botplay');
    if (botplay) {
      _player1 = Player1();
      _player2 = BotPlayer();
    } else {
      _player1 = Player1();
      _player2 = Player2();
    }
    _currentPlayer = _player1;
  }

  static void startGame({bool doReset: false}) async {
    if (doReset) {
      logger.i('Starting game with reset');
      reset();
    }
    await Future.delayed(Duration(seconds: 1));
    _currentPlayer.play();
  }

  static void changeToTestState() {
    _gridCellsState = [
      [_player2, _player1, _player2, _player1, _player2, _player1],
      [_player2, _player1, _player2, _player1, _player2, _player1],
      [_player2, _player1, _player2, _player1, null, null],
      [_player1, _player2, _player1, _player2, null, null],
      [_player2, _player1, _player2, _player1, null, null],
      [_player2, _player1, _player2, _player1, _player2, _player1],
      [_player2, _player1, _player2, _player1, _player2, _player1],
    ];
  }

  static List<List> getCellStateList() {
    return _gridCellsState;
  }

  static List<List> getConvertedCellList() {
    List<List> grid = [];
    for (List column in _gridCellsState) {
      List normalisedColumn = [];
      for (var state in column) {
        if (state != null) {
          normalisedColumn.add(state.getName());
        } else {
          normalisedColumn.add(null);
        }
      }
      grid.add(normalisedColumn);
    }
    return grid;
  }

  static Player getPlayer(int playerNumber) {
    if (playerNumber == 1) {
      return _player1;
    } else {
      return _player2;
    }
  }

  void addGameContext(BuildContext context) {
    /**
     * This method will add a context to this class
     * Mainly used to raise alert
     */
    _gameContext = context;
  }

  void addGridColumn(gridColumn, int columnPosition) {
    /**
     * Adds a column in grid
     */
    _gridColumns[columnPosition] = gridColumn;
  }

  String getRowColumn(int column, int row) {
    /**
     * This is a test function to get the RowColumn labels
     */
    return _gridCellsLabel[column][row];
  }

  Color getColor(int column, int row) {
    /**
     * This function will help determine color of a cell
     */
    Player occupier = _gridCellsState[column][row];
    if (occupier == null) {
      return Colors.transparent;
    } else {
      return occupier.getColor();
    }
  }

  bool checkListIsFilled(List list1, var item) {
    /**
     * A method to check if the entire list is filled with given item
     */
    for (int i = 0; i < list1.length; i++) {
      if (list1[i] != item) {
        return false;
      }
    }
    return true;
  }

  void raiseAlert(String title, String message) {
    /**
     * Used to raise alert at end of game
     */
    AlertStyle alertStyle = AlertStyle(
      animationType: AnimationType.grow,
      isCloseButton: false,
      isOverlayTapDismiss: false,
      descStyle: TextStyle(
        fontWeight: FontWeight.bold,
        color: Colors.white,
      ),
      animationDuration: Duration(milliseconds: 100),
      alertBorder: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
        side: BorderSide(
          color: Colors.white,
        ),
      ),
      titleStyle: TextStyle(
        color: Colors.yellow,
      ),
      backgroundColor: Colors.blueAccent,
    );
    Future.delayed(Duration(seconds: 1), () {
      Navigator.of(_gameContext).pop(true);
    });
    Alert(
      context: _gameContext,
      title: title,
      style: alertStyle,
      desc: message,
      closeFunction: () {},
      buttons: [],
    ).show();
  }

  bool checkIfDraw() {
    /**
     * Checks if all the cells are filled. If yes gives draw
     */
    bool filled = true;
    for (var column in _gridCellsState) {
      if (column.last == null) {
        filled = false;
        break;
      }
    }
    if (filled) {
      _player1.setDraw();
      _player2.setDraw();
      logger.i("This game is a draw");
      raiseAlert(
        "Draw!!!",
        "You both are awsome. This game is a tie",
      );
    } else {
      logger.d("Continue playing");
    }
    return filled;
  }

  Player getCellState(int column, int row) {
    /**
     * This will check if given cell position is valid
     * If valid return current cell state
     * If not a valid position returns outOfBound player
     */
    bool validColumn = (0 <= column) && (column < _columnCount);
    bool validRow = (0 <= row) && (row < _rowCount);
    if (validColumn && validRow) {
      return _gridCellsState[column][row];
    } else {
      return _outOfBoundsPlayer;
    }
  }

  bool checkIfWinningPattern(List stateList) {
    /**
     * From a statelist check if any 4 continues list is _currentPlayer
     */
    if (stateList.length < 4) {
      return false;
    }
    logger.d('Pattern >>> :$stateList');
    for (int i = 0; i < stateList.length - 3; i++) {
      if (checkListIsFilled(stateList.sublist(i, i + 4), _currentPlayer))
        return true;
    }
    return false;
  }

  bool checkIfWon(int column, int row) {
    /**
     * This function will check if the current move resulted a win
     */
    List vertical = [];
    List horizontal = [];
    List diaganolLeft = [];
    List diaganolRight = [];

    // This portion will check 3 cells above and below current cell
    // in all directions
    for (int offset = -3; offset < 4; offset++) {
      Player verticalPlayer = getCellState(column, row + offset);
      if (verticalPlayer != _outOfBoundsPlayer) vertical.add(verticalPlayer);

      Player horizontalPlayer = getCellState(column + offset, row);
      if (horizontalPlayer != _outOfBoundsPlayer)
        horizontal.add(horizontalPlayer);

      Player diaganolLeftPlayer = getCellState(column + offset, row + offset);
      if (diaganolLeftPlayer != _outOfBoundsPlayer)
        diaganolLeft.add(diaganolLeftPlayer);

      Player diaganolRightPlayer = getCellState(column + offset, row - offset);
      if (diaganolRightPlayer != _outOfBoundsPlayer)
        diaganolRight.add(diaganolRightPlayer);
    }
    if (checkIfWinningPattern(vertical) ||
        checkIfWinningPattern(horizontal) ||
        checkIfWinningPattern(diaganolLeft) ||
        checkIfWinningPattern(diaganolRight)) {
      String playerName = _currentPlayer.getName();
      _currentPlayer.setWin();
      raiseAlert('Game over', '$playerName has won the game');
      return true;
    } else {
      return false;
    }
  }

  bool isGameOver(column, row) {
    /**
     * Checks if game is over by checking if anyone has won or if no more moves
     */
    return (checkIfWon(column, row) || checkIfDraw());
  }

  int insertCoin(int column) {
    /**
     * This function implements the logic of inserting coin
     */
    if (_isGameOver) return null;
    for (int row = 0; row < _rowCount; row++) {
      if (_gridCellsState[column][row] == null) {
        _gridCellsState[column][row] = _currentPlayer;
        if (isGameOver(column, row)) {
          _isGameOver = true;
          // Uncomment below for bot vs bot continues play
          // startGame(doReset: true);
        } else {
          changeCurrentPlayer();
        }
        return row;
      }
    }
    isGameOver(column, _rowCount);
    return null;
  }

  static void popCoin(int column, int row) async {
    /**
     * This function implements the logic of inserting coin
     */
    // if (_isGameOver) return null;
    // _gridCellsState[column][row] = null;
    // await Future.delayed(const Duration(milliseconds: 100));
    _gridCellsState[column][row] = null;
    _gridColumns[column].refreshCell(row);
    logger.d('Popping coin from $column, $row');
  }

  static void changeCurrentPlayer() {
    /**
     * This function will change current player
     */
    _currentPlayer.toggleTurn();
    _currentPlayer = _currentPlayer.getOpponent();
    _currentPlayer.toggleTurn();
  }

  static void currentPlayerPlay() async {
    // await Future.delayed(const Duration(milliseconds: 50));
    if (!_isGameOver) {
      _currentPlayer.play();
    }
  }

  static bool isCurrrentPlayerBot() {
    return _currentPlayer.areYouBot();
  }

  static void simulateClick(int column) {
    /**
     * Simulates button click on the specified column
     */
    logger.d('simulating clicking column $column');
    _gridColumns[column].clickButton();
  }

  static void reset() async {
    /**
     * Resets the game state to new game
     */
    logger.w('Resetting the game');
    _isGameOver = false;
    if (_gridCellsState.length > 0) {
      for (int column = 0; column < _columnCount; column++) {
        for (int row = 0; row < _rowCount; row++) {
          _gridCellsState[column][row] = null;
        }
      }
      logger.d(_gridCellsState);
      logger.d(_gridColumns);
      // Refreshes each column
      for (var coloumn in _gridColumns) {
        coloumn.refreshState();
      }
      // changeToTestState();
    }
    // This delay is required for proper rendering in case of
    // first turn by the bot player
    await Future.delayed(const Duration(milliseconds: 50));
    _player1.reset();
    _player2.reset();
    _currentPlayer.toggleTurn();
    _currentPlayer.play();
  }

  static void setGridCellLabel(column, row, label) {
    /**
     * Set any label for a grid.
     */
    _gridCellsLabel[column][row] = label;
    _gridColumns[column].refreshCell(row);
  }

  static int getRowCount() {
    return _rowCount;
  }

  static int getColumnCount() {
    return _columnCount;
  }
}
