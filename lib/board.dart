import 'package:flutter/material.dart';
import 'coin_grid.dart';
import 'player_card.dart';

class Connect4GameBoard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget board;
    if (MediaQuery.of(context).size.height <
        MediaQuery.of(context).size.width) {
      board = Row(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Expanded(
            child: PlayerCard(1),
          ),
          CoinGrid(),
          Expanded(
            child: PlayerCard(2),
          ),
        ],
      );
    } else {
      board = Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Expanded(
            child: PlayerCard(1),
          ),
          CoinGrid(),
          Expanded(
            child: PlayerCard(2),
          ),
        ],
      );
    }
    return board;
  }
}
