import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'connect_brain.dart';

Connect4Engine gameEngine;
Logger logger = Logger();

class CoinGrid extends StatefulWidget {
  @override
  // _CoinGridState createState() => _CoinGridState(3, 3);
  _CoinGridState createState() => _CoinGridState(
        Connect4Engine.getRowCount(),
        Connect4Engine.getColumnCount(),
      );
}

class _CoinGridState extends State<CoinGrid> {
  int _rowCount, _columnCount;
  // This list represents each column on the board
  List<GridColumn> _gridColoumn = [];

  _CoinGridState(this._rowCount, this._columnCount) {
    gameEngine = Connect4Engine();
    for (int column = 0; column < this._columnCount; column++) {
      _gridColoumn.add(GridColumn(column, this._rowCount));
    }
  }

  @override
  Widget build(BuildContext context) {
    gameEngine.addGameContext(context);
    return AspectRatio(
      aspectRatio: _columnCount / _rowCount,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: _gridColoumn,
      ),
    );
  }
}

class GridColumn extends StatefulWidget {
  int _columnNumber, _rowCount;

  GridColumn(this._columnNumber, this._rowCount);
  @override
  _GridColumnState createState() => _GridColumnState(_columnNumber, _rowCount);
}

class _GridColumnState extends State<GridColumn> {
  int _columnNumber, _rowCount;
  static bool clickNotAllowed = false;
  // Here the grid cell represents cells/rows in a single coloumn
  // Please not here the First item will be in reverse order wrt gameEngine
  List<GridCell> _gridCells = [];

  _GridColumnState(this._columnNumber, this._rowCount) {
    for (int row = this._rowCount - 1; row >= 0; row--) {
      _gridCells.add(GridCell(this._columnNumber, row));
    }
    // Adds the self object to gameEngine for bot players to clickButton
    gameEngine.addGridColumn(this, this._columnNumber);
  }

  void clickButton() async {
    clickNotAllowed = true;
    logger.i('pressed column $_columnNumber');
    int rowNumber = gameEngine.insertCoin(_columnNumber);
    if (rowNumber != null) {
      for (int row = _rowCount; row > rowNumber; row--) {
        if (this.mounted) {
          setState(() {
            // Caclulating localRowCount since the orde is in reverse
            int localRowCount = _rowCount - row;
            if (localRowCount > 0) {
              // Resetting to orginal one if not the final cell
              _gridCells[localRowCount - 1] = GridCell(
                _columnNumber,
                row,
              );
            }
            // If not final cell setting temporarly for animation
            _gridCells[localRowCount] = GridCell(_columnNumber, rowNumber);
          });
        }
        // To create animation effect. Keep coin in each cell fro 100ms
        await Future.delayed(const Duration(milliseconds: 100));
      }
    } else {
      logger.d('No more cell to insert coin');
    }
    clickNotAllowed = false;
    Connect4Engine.currentPlayerPlay();
  }

  void refreshCell(row) {
    setState(() {
      _gridCells[_rowCount - row - 1] = GridCell(_columnNumber, row);
    });
  }

  void refreshState() {
    logger.d('Refreshing column $_columnNumber');
    clickNotAllowed = false;
    if (this.mounted) {
      setState(() {
        _gridCells = [];
        for (int row = this._rowCount - 1; row >= 0; row--) {
          _gridCells.add(GridCell(this._columnNumber, row));
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: FlatButton(
        padding: EdgeInsets.all(0),
        onPressed: () {
          if (Connect4Engine.isCurrrentPlayerBot() || clickNotAllowed) {
            logger.d('Clicking ignored');
            return;
          }
          clickButton();
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: _gridCells,
        ),
      ),
    );
  }
}

class GridCell extends StatelessWidget {
  int _columnNumber, _rowNumber;

  GridCell(this._columnNumber, this._rowNumber);
  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 1,
      child: Container(
        color: gameEngine.getColor(_columnNumber, _rowNumber),
        foregroundDecoration: BoxDecoration(
          image: DecorationImage(
            fit: BoxFit.contain,
            image: AssetImage('images/board.png'),
          ),
        ),
        // child: Center(
        //   child: Text(
        //     // This string and return string should match.
        //     // Don't remove this. Could be used for testing
        //     // '$_columnNumber,$_rowNumber',
        //     gameEngine.getRowColumn(_columnNumber, _rowNumber),
        //     style: TextStyle(
        //       color: Colors.black,
        //     ),
        //   ),
        // ),
      ),
    );
  }
}
