import 'package:logger/logger.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:connect_4_flutter/board.dart';
import 'package:connect_4_flutter/connect_brain.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  Logger.level = Level.info;
  runApp(Connect4Canvas());
}

class Connect4Canvas extends StatelessWidget {
  void _setToLandscape() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.landscapeRight,
    ]);
  }

  void _updateAppbar() {
    SystemChrome.setEnabledSystemUIOverlays([]);
  }

  @override
  Widget build(BuildContext context) {
    _setToLandscape();
    _updateAppbar();
    return MaterialApp(
      theme: ThemeData.dark(),
      home: GameMode(),
      title: "Connect 4",
    );
  }
}

class GameMode extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Expanded(
              child: FlatButton(
                onPressed: () {
                  Connect4Engine.setGameMode(false);
                  // Navigator.pop(context);
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Connect4Home(),
                    ),
                  );
                },
                color: Colors.red,
                textColor: Colors.black,
                child: Text('Player VS Player'),
              ),
            ),
            Expanded(
              child: FlatButton(
                onPressed: () {
                  Connect4Engine.setGameMode(true);
                  // Navigator.pop(context);
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Connect4Home(),
                    ),
                  );
                },
                color: Colors.yellowAccent.shade700,
                textColor: Colors.black,
                child: Text('Player VS Bot'),
              ),
            ),
          ],
        ),
      ),
      backgroundColor: Colors.deepPurpleAccent,
    );
  }
}

class Connect4Home extends StatefulWidget {
  @override
  _Connect4HomeState createState() => _Connect4HomeState();
}

class _Connect4HomeState extends State<Connect4Home> {
  // @override
  // void initState() {
  //   _updateAppbar();
  //   _setToLandscape();
  //   super.initState();
  // }

  // void _setToLandscape() {
  //   SystemChrome.setPreferredOrientations([
  //     DeviceOrientation.landscapeLeft,
  //     DeviceOrientation.landscapeRight,
  //   ]);
  // }

  // void _updateAppbar() {
  //   SystemChrome.setEnabledSystemUIOverlays([]);
  // }

  var scaffoldKey = GlobalKey<ScaffoldState>();

  void raiseAlert(BuildContext context) {
    AlertStyle alertStyle = AlertStyle(
      animationType: AnimationType.fromBottom,
      isCloseButton: true,
      isOverlayTapDismiss: true,
      constraints: BoxConstraints(
        maxHeight: MediaQuery.of(context).size.height * .8,
      ),
      descStyle: TextStyle(
        fontWeight: FontWeight.normal,
        color: Colors.white,
        fontSize: MediaQuery.of(context).size.height * .05,
      ),
      animationDuration: Duration(milliseconds: 100),
      alertBorder: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
        side: BorderSide(
          color: Colors.white,
        ),
      ),
      titleStyle: TextStyle(
        color: Colors.yellow,
        fontSize: MediaQuery.of(context).size.height * .05,
        fontWeight: FontWeight.bold,
      ),
      backgroundColor: Colors.blueAccent,
      buttonAreaPadding: EdgeInsets.all(10),
    );
    Alert(
      context: context,
      title: 'How to play',
      style: alertStyle,
      image: Image(
        height: MediaQuery.of(context).size.height * .3,
        image: AssetImage('images/guide.gif'),
      ),
      desc: 'Press any of the columns to drop your coin to that cloumn',
      closeFunction: () {},
      buttons: [
        DialogButton(
          color: Colors.yellow,
          height: MediaQuery.of(context).size.height * .07,
          child: Text(
            "DISMISS",
            style: TextStyle(
              color: Colors.deepOrangeAccent,
              fontSize: MediaQuery.of(context).size.height * .05,
            ),
          ),
          onPressed: () {
            // This is to remove the alert box
            Navigator.pop(context);
          },
          width: MediaQuery.of(context).size.height * .05 * 5,
        ),
      ],
    ).show();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      drawer: Drawer(
        child: SafeArea(
          right: false,
          child: Column(
            children: [
              FlatButton(
                color: Colors.blue,
                child: Text("Home"),
                minWidth: 100,
                onPressed: () {
                  Navigator.pop(context);
                  Navigator.pop(context);
                },
              ),
              FlatButton(
                color: Colors.blue,
                child: Text("New Game"),
                minWidth: 100,
                onPressed: () {
                  Connect4Engine.reset();
                  Navigator.pop(context);
                },
              ),
              FlatButton(
                color: Colors.blue,
                child: Text("Game Help"),
                minWidth: 100,
                onPressed: () {
                  Navigator.pop(context);
                  raiseAlert(context);
                },
              )
            ],
          ),
        ),
      ),
      body: SafeArea(
        child: Stack(
          children: [
            Connect4GameBoard(),
            Positioned(
              left: 10,
              top: 10,
              child: IconButton(
                icon: Icon(
                  Icons.menu,
                  size: 40,
                ),
                onPressed: () => scaffoldKey.currentState.openDrawer(),
              ),
            ),
          ],
        ),
      ),
      backgroundColor: Colors.deepPurpleAccent,
    );
  }
}
