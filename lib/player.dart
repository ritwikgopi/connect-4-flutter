import 'package:flutter/material.dart';

import 'bot_brain.dart';

class Player {
  Color _coinColor = Colors.transparent;
  String _name;
  Player _opponent;
  var _playerCard;

  Color getColor() {
    return _coinColor;
  }

  void setPlayerCard(playerCard) {
    /**
     * This function will be used to get the context of playercard widget
     * So we can call toggleTurn
     */
    _playerCard = playerCard;
  }

  void toggleTurn() {
    _playerCard.toggleTurn();
  }

  void setWin() {
    _playerCard.setWin();
    getOpponent().setLost();
  }

  void setDraw() {
    _playerCard.setDraw();
  }

  void setLost() {
    _playerCard.setLost();
  }

  void reset() {
    _playerCard.reset();
  }

  void addOppenent(opponent) {
    _opponent = opponent;
  }

  void play() {}

  bool areYouBot() {
    return false;
  }

  String getName() {
    return _name;
  }

  Player getOpponent() {
    return _opponent;
  }
}

class Player1 extends Player {
  Player1() {
    super._coinColor = Colors.red;
    super._name = "Player 1";
  }
}

class Player2 extends Player {
  Player2() {
    super._coinColor = Colors.yellow;
    super._name = "Player 2";
  }
}

class BotPlayer extends Player {
  BotPlayer() {
    super._coinColor = Colors.yellow;
    super._name = "Bot";
  }

  @override
  void play() {
    BotBrain.playNextMove(this);
  }

  @override
  bool areYouBot() {
    return true;
  }
}

class BotPlayer1 extends BotPlayer {
  BotPlayer1() {
    super._coinColor = Colors.red;
    super._name = "Bot 1";
  }
}

class BotPlayer2 extends BotPlayer {
  BotPlayer2() {
    super._coinColor = Colors.yellow;
    super._name = "Bot 2";
  }
}
