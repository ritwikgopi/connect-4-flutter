import 'package:flutter/foundation.dart';
import 'package:logger/logger.dart';
import 'dart:math';

import 'connect_brain.dart';

class BotBrain {
  static final Logger logger = Logger();
  static String _maximizingPlayer, _minimizingPlayer;
  static void playNextMove(currentPlayer) {
    /**
     * This function will take care of calling the negamax to find
     * next move and performing it
     */
    logger.d("Play next move is called for $currentPlayer");
    _maximizingPlayer = currentPlayer.getName();
    _minimizingPlayer = currentPlayer.getOpponent().getName();
    List<List> currentCellStates = Connect4Engine.getConvertedCellList();
    Map computData = {
      'currentCellStates': currentCellStates,
      '_maximizingPlayer': _maximizingPlayer,
      '_minimizingPlayer': _minimizingPlayer
    };
    compute(getNextMove, computData)
        .then((move) => Connect4Engine.simulateClick(move));
    // Connect4Engine.simulateClick(getNextMove(_maximizingPlayer));
    return;
  }

  static int getNextMove(Map data) {
    Logger.level = Level.info;
    _maximizingPlayer = data['_maximizingPlayer'];
    _minimizingPlayer = data['_minimizingPlayer'];
    List<List> currentCellStates = data['currentCellStates'];
    logger.i('Starting a compute loop for $_maximizingPlayer');
    logger.d(currentCellStates);
    // Passing max for maximising turn
    List minmaxout = negamax(currentCellStates, max, _maximizingPlayer);
    logger.i('####################### $minmaxout');
    return minmaxout[1];
  }

  static getOpponent(currentPlayer) {
    if (currentPlayer == _maximizingPlayer) {
      return _minimizingPlayer;
    } else {
      return _maximizingPlayer;
    }
  }

  static bool checkIfTie(state) {
    /**
     * Checks if current state is a tie
     */
    bool draw = true;
    for (List column in state) {
      if (column.last == null) {
        draw = false;
        break;
      }
    }
    return draw;
  }

  static String getCellState(List state, int column, int row) {
    /**
     * This will check if given cell position is valid
     * If valid return current cell state
     * If not a valid position returns outOfBound player
     */
    int _columnCount = state.length;
    int _rowCount = state[0].length;
    bool validColumn = (0 <= column) && (column < _columnCount);
    bool validRow = (0 <= row) && (row < _rowCount);
    if (validColumn && validRow) {
      return state[column][row];
    } else {
      return 'InvalidPosition';
    }
  }

  static bool checkListIsFilled(List list1, var item) {
    /**
     * A method to check if the entire list is filled with given item
     */
    for (int i = 0; i < list1.length; i++) {
      if (list1[i] != item) {
        return false;
      }
    }
    return true;
  }

  static bool checkIfWinningPattern(List stateList, String currentPlayer) {
    /**
     * From a statelist check if any 4 continues list is _currentPlayer
     */
    if (stateList.length < 4) {
      return false;
    }
    // logger.d('Pattern >>> :$stateList');
    for (int i = 0; i < stateList.length - 3; i++) {
      if (checkListIsFilled(stateList.sublist(i, i + 4), currentPlayer))
        return true;
    }
    return false;
  }

  static List checkIfWon(List state, int column, int row) {
    /**
     * This function will check if the current move resulted a win
     */
    List vertical = [];
    List horizontal = [];
    List diaganolLeft = [];
    List diaganolRight = [];

    String currentPlayer = state[column][row];

    // This portion will check 3 cells above and below current cell
    // in all directions
    for (int offset = -3; offset < 4; offset++) {
      String verticalPlayer = getCellState(
        state,
        column,
        row + offset,
      );
      if (verticalPlayer != 'InvalidPosition') vertical.add(verticalPlayer);

      String horizontalPlayer = getCellState(
        state,
        column + offset,
        row,
      );
      if (horizontalPlayer != 'InvalidPosition')
        horizontal.add(horizontalPlayer);

      String diaganolLeftPlayer = getCellState(
        state,
        column + offset,
        row + offset,
      );
      if (diaganolLeftPlayer != 'InvalidPosition')
        diaganolLeft.add(diaganolLeftPlayer);

      String diaganolRightPlayer = getCellState(
        state,
        column + offset,
        row - offset,
      );
      if (diaganolRightPlayer != 'InvalidPosition')
        diaganolRight.add(diaganolRightPlayer);
    }
    return [
      (checkIfWinningPattern(vertical, currentPlayer) ||
          checkIfWinningPattern(horizontal, currentPlayer) ||
          checkIfWinningPattern(diaganolLeft, currentPlayer) ||
          checkIfWinningPattern(diaganolRight, currentPlayer)),
      {
        "vertical": vertical,
        "horizontal": horizontal,
        "diaganolLeft": diaganolLeft,
        "diaganolRight": diaganolRight,
      }
    ];
  }

  static bool terminalState(List<List> state, int lastColumn, int lastRow) {
    /**
     * This function checks if the game reached terminal state
     * Terminal states are
     * Either of the player winning the game
     * Or the game becoming draw
     */
    if (lastColumn == null || lastRow == null) {
      logger.d('First move assume the opponent hasn\'t won');
      return false;
    }
    List hasWonOutput = checkIfWon(state, lastColumn, lastRow);
    bool hasWon = hasWonOutput[0];
    if (hasWon) {
      return hasWon;
    }
    bool isTie = checkIfTie(state);
    return isTie;
  }

  static double calculatePossibleScore(state, column, row, possibleWinStates) {
    double score = 0;
    String currentPlayer = state[column][row];
    // This for loop will check win state in all possible directions
    for (var winState in possibleWinStates) {
      if (winState.length < 4) {
        continue;
      }
      // This loop is to generate sub states with 4 coins
      for (int i = 0; i < winState.length - 3; i++) {
        List subsetWinState = winState.sublist(i, i + 4);
        double tempScore = 0;
        // This Logic will pick a 4 continues state out of possible win states
        // and if the coin is same player or empty point will be added
        // If any opponent coin is present it will break the loop
        for (String coin in subsetWinState) {
          if (coin == currentPlayer) {
            tempScore += 2;
          } else if (coin == null) {
            tempScore += 1;
          } else {
            tempScore = 0;
            break;
          }
        }
        score += tempScore;
      }
    }
    return score;
  }

  static double getScore(
      List<List> state, int lastColumn, int lastRow, Function function) {
    /**
     * This function will return a score for the game.
     */
    logger.d('getting score for $function');
    List hasWonOutput = checkIfWon(state, lastColumn, lastRow);
    bool hasWon = hasWonOutput[0];
    if (hasWon) {
      logger.d('getting score for $hasWon');
      // For maximising player will return 2 as score
      // For minimising player will return -2 as score
      return function(100.0, -100.0);
    }
    if (checkIfTie(state)) {
      return 0.0;
    }
    double score = calculatePossibleScore(
      state,
      lastColumn,
      lastRow,
      hasWonOutput[1].values,
    );
    logger.d('Got a possible score of $score');
    return function(score, -score);
  }

  static List<int> getValidMoves(List<List> state) {
    /**
     * This function will generate next set of valid moves
     */
    List<int> validMoves = [];
    for (int column = 0; column < state.length; column++) {
      if (state[column].last == null) validMoves.add(column);
    }
    validMoves.shuffle();
    return validMoves;
  }

  static int insertCoin(List state, int column, String currentPlayer) {
    /**
     * This function implements the logic of inserting coin
     */
    int _rowCount = state[column].length;
    for (int row = 0; row < _rowCount; row++) {
      if (state[column][row] == null) {
        state[column][row] = currentPlayer;
        return row;
      }
    }
    return null;
  }

  static Function getNextFunction(Function currentFunction) {
    /**
     * This function to compement between min and max
     */
    if (currentFunction == max) {
      return min;
    } else {
      return max;
    }
  }

  static dynamic negamax(
      List<List> state, Function function, String currentPlayer,
      {int depth: 0,
      int lastColumn,
      int lastRow,
      double alpha: -double.infinity,
      double beta: double.infinity,
      double maxDepth: 6}) {
    if (terminalState(state, lastColumn, lastRow) || depth >= maxDepth) {
      double score = getScore(
        state,
        lastColumn,
        lastRow,
        // We get the opposite function since actually we are checking
        // If the last move resulted in a terminal state.
        // If it did it should rerpresent the score for that player.
        getNextFunction(function),
      );
      // Multiply socre by the below to give the
      // terminal state with less depth more score
      score = score * (state.length * state[0].length - depth);
      logger.d('Depth - $depth, Returning terminal state $score, $lastColumn.');
      return [score, lastColumn];
    }
    double bestScore = -function(double.infinity, -double.infinity);
    List<int> validMoves = getValidMoves(state);
    int bestMove;
    logger.d('Bestscore is $bestScore, validMoves: $validMoves');
    for (int move in validMoves) {
      logger.d(
          'Depth: $depth - Checking $function for $validMoves for move $move');
      int insertedRow = insertCoin(state, move, currentPlayer);

      // Below code is for debugging purpose.
      // Uncomment it to slow down the minmax calculation and view on the UI

      // Connect4Engine.setGridCellLabel(
      //   move,
      //   insertedRow,
      //   '$alpha-$beta,$depth',
      // );
      // await Future.delayed(const Duration(milliseconds: 50));
      List scoreAndMove = negamax(
        state,
        getNextFunction(function),
        getOpponent(currentPlayer),
        depth: depth + 1,
        lastColumn: move,
        lastRow: insertedRow,
        alpha: alpha,
        beta: beta,
      );
      state[move][insertedRow] = null;
      double oldBestScore = bestScore;
      bestScore = function(scoreAndMove[0], bestScore);
      if (function == max) {
        alpha = function(alpha, bestScore);
      } else {
        beta = function(beta, bestScore);
      }
      logger.d('alpha: $alpha, beta: $beta, currentFunction: $function, ' +
          'Depth: $depth bestScore: $bestScore, ' +
          'bestMove: $bestMove, currentMove: $move');
      if (beta <= alpha) {
        logger.d("^^^^^^^^^^^ Pruning at depth - $depth for move - $move");
        bestMove = move;
        break;
      }
      double newScore = scoreAndMove[0];
      if (oldBestScore != bestScore) {
        bestMove = move;
      }
      logger.d('Depth: $depth - Oldscore $oldBestScore, newScore $newScore,' +
          'bestScore: $bestScore, bestMove: $bestMove, currentMove: $move');

      // Below code is for debugging purpose.
      // Uncomment it to slow down the minmax calculation and view on the UI

      // Connect4Engine.setGridCellLabel(
      //   move,
      //   insertedRow,
      //   '$alpha-$beta,$depth',
      // );
      // Connect4Engine.popCoin(move, insertedRow);
      // await Future.delayed(const Duration(milliseconds: 50));
    }
    logger.d('Depth: $depth - final bestScore: $bestScore, ' +
        'bestMove: $bestMove, current: $lastColumn, ' +
        'alpha: $alpha, beta: $beta');
    return [bestScore, bestMove];
  }
}
