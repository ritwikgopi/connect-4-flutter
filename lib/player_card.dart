import 'package:connect_4_flutter/connect_brain.dart';
import 'package:flutter/material.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'player.dart';

class PlayerCard extends StatefulWidget {
  int _playerNumber;
  PlayerCard(this._playerNumber);
  @override
  _PlayerCardState createState() => _PlayerCardState(_playerNumber);
}

class _PlayerCardState extends State<PlayerCard> {
  Player _player;
  double _backgroundOpacity = 0;
  bool _restartVisibility = false;
  bool _winVisibility = false, _lostVisibility = false, _drawVisibility = false;
  bool _turnVisibility = false, _waitVisibility = true;
  int _winCount = 0, _loseCount = 0, _drawCount = 0;
  _PlayerCardState(playerNumber) {
    if (playerNumber == 1) {
      _player = Connect4Engine.getPlayer(1);
    } else {
      _player = Connect4Engine.getPlayer(2);
    }
  }

  void toggleTurn() {
    /**
     * This functions toggles between Active or inactive status of a player
     */
    setState(() {
      _turnVisibility = !_turnVisibility;
      _waitVisibility = !_waitVisibility;
      if (_backgroundOpacity == 0)
        _backgroundOpacity = .5;
      else
        _backgroundOpacity = 0;
    });
  }

  void setWin() {
    /**
     * this funcion to be called when the player wins
     */
    setState(() {
      _waitVisibility = false;
      _turnVisibility = false;
      _winVisibility = true;
      _lostVisibility = false;
      _drawVisibility = false;
      _restartVisibility = true;
      _winCount++;
    });
  }

  void setLost() {
    /**
     * This function to be called when player looses
     */
    setState(() {
      _waitVisibility = false;
      _turnVisibility = false;
      _winVisibility = false;
      _lostVisibility = true;
      _drawVisibility = false;
      _restartVisibility = true;
      _loseCount++;
    });
  }

  void setDraw() {
    /**
     * This function to be called when player looses
     */
    setState(() {
      _waitVisibility = false;
      _turnVisibility = false;
      _winVisibility = false;
      _lostVisibility = false;
      _drawVisibility = true;
      _restartVisibility = true;
      _drawCount++;
    });
  }

  void reset() {
    /**
     * This function resets the state to initial
     */
    setState(() {
      _waitVisibility = true;
      _turnVisibility = false;
      _winVisibility = false;
      _lostVisibility = false;
      _restartVisibility = false;
      _drawVisibility = false;
      _backgroundOpacity = 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    _player.setPlayerCard(this);
    return Container(
      decoration: BoxDecoration(
        color: Color.fromRGBO(0, 0, 0, _backgroundOpacity),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          AutoSizeText(
            _player.getName(),
            maxFontSize: 30,
            maxLines: 1,
            style: TextStyle(
              color: _player.getColor(),
              fontSize: 30,
              fontWeight: FontWeight.bold,
            ),
          ),
          Visibility(
            visible: _turnVisibility,
            child: AutoSizeText(
              'Play',
              maxLines: 1,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.lightGreenAccent.shade400,
                fontSize: 23,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Visibility(
            visible: _waitVisibility,
            child: AutoSizeText(
              'Wait',
              textAlign: TextAlign.center,
              maxLines: 1,
              style: TextStyle(
                color: Colors.deepOrange.shade300,
                fontSize: 23,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Visibility(
            visible: _winVisibility,
            child: AutoSizeText(
              'WON THE GAME',
              maxLines: 1,
              style: TextStyle(
                color: Colors.lightGreenAccent.shade400,
                fontSize: 30,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Visibility(
            visible: _lostVisibility,
            child: AutoSizeText(
              'LOST THE GAME',
              maxLines: 1,
              style: TextStyle(
                color: Colors.red,
                fontSize: 30,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Visibility(
            visible: _drawVisibility,
            child: AutoSizeText(
              'THIS GAME IS A TIE',
              maxLines: 1,
              style: TextStyle(
                color: Colors.white,
                fontSize: 30,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          AutoSizeText(
            '$_winCount-$_drawCount-$_loseCount',
            maxFontSize: 30,
            maxLines: 1,
            style: TextStyle(
              color: Colors.white,
              fontSize: 30,
              fontWeight: FontWeight.bold,
            ),
          ),
          Visibility(
            visible: _restartVisibility,
            child: CircleAvatar(
              backgroundColor: _player.getColor(),
              child: IconButton(
                onPressed: () {
                  setState(() {
                    Connect4Engine.reset();
                    _restartVisibility = false;
                    _winVisibility = false;
                  });
                },
                icon: Icon(
                  Icons.replay,
                  color: Colors.blueAccent,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
